//
//  AppDelegate.m
//  Example
//
//  Created by sujeking on 2022/7/28.
//

#import "AppDelegate.h"

#import <CDNetModule/CDNetModule.h>

@interface AppDelegate ()<CDNetModuleRequestDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [CDNetModuleSetup setupWithHosts:@"https://suggest.taobao.com"];
    
    [CDNetModuleSetup sharedInstance].delegate = self;
    
    
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}

///mark - CDNetModuleRequestDelegate

- (void)requestFinishHandleWithData:(id)json
                            success:(void (^)(ZCErrorCode, NSString *, NSDictionary *))success
                            failure:(void (^)(ZCErrorCode, NSString *, NSDictionary *))failure {
    success(200,@"success",json);
}


@end
