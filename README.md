## 使用说明


设置服务器地址
    
```
[CDNetModuleSetup setupWithHosts:@"https://example.com"];
```

实现 `CDNetModuleRequestDelegate` 接口 实现自己的业务

```
- (void)requestFinishHandleWithData:(id)json success:(void (^)(ZCErrorCode, NSString *,NSDictionary *))success failure:(void (^)(ZCErrorCode, NSString *, NSDictionary *))failure {
    //json
    success(200,@"success",json);
}

```

设置header

```
[CDNetModuleSetup sharedInstance].header = @{....}

```
清除header

```
[CDNetModuleSetup sharedInstance].resetHeaderKeys = @[...]

```

使用

```
[SKBaseWebService getRequest:@"/sug" parameters:@{@"code":@"utf-8",@"q":@"卫衣"} progress:nil success:^(ZCErrorCode status, NSString *msg, NSDictionary *data) {

    } failure:^(ZCErrorCode status, NSString *msg, NSDictionary *data) {

    }];

```
其他
设置info.plist

```
<key>NSAppTransportSecurity</key>
    <dict>
        <key>NSAllowsArbitraryLoads</key>
    <true/>
</dict>
```


