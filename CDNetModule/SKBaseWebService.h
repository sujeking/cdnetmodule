//
//  CDCTWebService.h
//  ChronicDiseaseManager
//
//  Created by CDCT on 2017/10/17.
//  Copyright © 2017年 cdct. All rights reserved.
//
//*************************************************
//网络请求模块封装
//
//与服务器预定数据格式如下
//必须含有这个3个key
//
//  {
//      code:"200",
//      msg:"服务器返回内容",
//      data:{}
//  }
//
//*************************************************

#import <Foundation/Foundation.h>

typedef int ZCErrorCode;

@interface SKBaseWebService : NSObject

+ (NSURLSessionDataTask *)postRequest:(NSString*)path
                           parameters:(id)parameters
                             progress:(void (^)(NSProgress *))uploadProgress
                              success:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))success
                              failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure;

+ (NSURLSessionDataTask *)getRequest:(NSString*)path
                          parameters:(NSDictionary*)parameters
                            progress:(void (^)(NSProgress *))downloadProgress
                             success:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))success
                             failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure;

+ (NSURLSessionDataTask *)uploadRequest:(NSString*)path
                             parameters:(NSDictionary*)parameters
                          formDataArray:(NSArray *)formDataArray
                               progress:(void (^)(NSProgress *))uploadProgress
                                success:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))success
                                failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure;

+ (NSURLSessionDataTask *)putRequest:(NSString*)path
                          parameters:(NSDictionary*)parameters
                             success:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))success
                             failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure;

+ (NSURLSessionDataTask *)deleteRequest:(NSString*)path
                             parameters:(NSDictionary*)parameters
                                success:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))success
                                failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure;


+ (NSURLSessionDataTask *)thirdRequest:(NSString *)path
                                  urls:(NSString *)url
                                  post:(BOOL)ispost
                            parameters:(NSDictionary*)parameters
                              progress:(void (^)(NSProgress * _Nonnull))downloadProgress
                               success:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))success
                               failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure;
@end
