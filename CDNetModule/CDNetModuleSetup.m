//
//  CDNetModuleSetup.m
//  CDNetModule
//
//  Created by sujeking on 2022/7/29.
//

#import "CDNetModuleSetup.h"

@implementation CDNetModuleSetup

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static CDNetModuleSetup *instance = nil;
    dispatch_once(&onceToken,^{
        instance = [[CDNetModuleSetup alloc] init];
    });
    return instance;
}

+ (void)setupWithHosts:(NSString *)url {
    [CDNetModuleSetup sharedInstance].baseURI = url;
}

@end
