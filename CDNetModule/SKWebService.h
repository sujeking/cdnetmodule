//
//  PEWebService.h
//  ChronicDiseaseManager
//
//  Created by CDCT on 2017/10/17.
//  Copyright © 2017年 cdct. All rights reserved.
//
//*************************************************
//框架 网络请求封装
//*************************************************


#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface SKWebService : NSObject

- (instancetype)initWebServiceWithUserAgent:(NSString *)userAgentStr
                                   deviceID:(NSString *)deviceIDStr;

- (AFHTTPSessionManager *)createSessionManagerWithUrl:(NSString *)urlString;

- (void)updateRequestSerializerHeadFieldWithDic:(NSDictionary *)dic baseURL:(NSString *)baseUrlStr;

- (NSURLSessionDataTask *)postWithBaseUrl:(NSString *)baseUrlStr
                                     path:(NSString *)path
                               parameters:(id)parameters
                                 progress:(void (^)(NSProgress *))uploadProgress
                                  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)getWithBaseUrl:(NSString *)baseUrlStr
                                    path:(NSString *)path
                              parameters:(id)parameters
                                progress:(void (^)(NSProgress *))uploadProgress
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)uploadWithBaseUrl:(NSString *)baseUrlStr
                                       path:(NSString *)path
                                 parameters:(NSDictionary *)parameters
                              formDataArray:(NSArray *)formDataArray
                                   progress:(void (^)(NSProgress *))uploadProgress
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask * )putWithBaseUrl:(NSString *)baseUrlStr
                                     path:(NSString *)path
                               parameters:(NSDictionary *)parameters
                                  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionDataTask *)deleteWithBaseUrl:(NSString *)baseUrlStr
                                       path:(NSString *)path
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;



@end
