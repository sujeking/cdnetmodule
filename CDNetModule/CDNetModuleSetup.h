//
//  CDNetModuleSetup.h
//  CDNetModule
//
//  Created by sujeking on 2022/7/29.
//

#import <Foundation/Foundation.h>


typedef int ZCErrorCode;

@protocol CDNetModuleRequestDelegate <NSObject>
/// 业务逻辑处理。主要是code
- (void)requestFinishHandleWithData:(id)json
                            success:(void (^)(ZCErrorCode code, NSString *msg, NSDictionary * data))success
                            failure:(void (^)(ZCErrorCode code, NSString *msg, NSDictionary * data))failure;
@end



NS_ASSUME_NONNULL_BEGIN

@interface CDNetModuleSetup : NSObject
///业务逻辑需要实现这个接口
@property(nonatomic,weak) id<CDNetModuleRequestDelegate> delegate;
/// 服务器地址 自行环境配置
@property (nonatomic, copy) NSString *baseURI;
/// 公共参数 例如设备信息、版本信息
@property (nonatomic, copy) NSDictionary *commonPramater;
/// header 设置  通常配置token
@property (nonatomic, copy) NSDictionary *header;
/// 要清空的header key
@property (nonatomic, copy) NSArray *resetHeaderKeys;

+ (instancetype)sharedInstance;

+ (void)setupWithHosts:(NSString *)url;


@end

NS_ASSUME_NONNULL_END
