//
//  CDCTWebService.m
//  ChronicDiseaseManager
//
//  Created by CDCT on 2017/10/17.
//  Copyright © 2017年 cdct. All rights reserved.
//

#import "SKBaseWebService.h"
#import "SKWebService.h"
#import "CDNetModuleSetup.h"

NSString * MainPath =@"";

static const NSArray<NSString *> *specialURLsArr;

@interface SKBaseWebService()

@end

@implementation SKBaseWebService

+ (SKWebService *)webService {
    SKWebService *webservice = [[SKWebService alloc] initWebServiceWithUserAgent:@"" deviceID:@""];
    return webservice;
}

#pragma mark - Core

+ (void)handleSuccess:(BOOL)isPost
                 path:(NSString*)path
           parameters:(NSDictionary*)parameters
            operation:(NSURLSessionDataTask *)op
                 json:(id)json
              success:(void (^)(ZCErrorCode status, NSString * msg, NSDictionary * data))success
              failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure {
    
    if ([CDNetModuleSetup sharedInstance].delegate == nil) {
        NSAssert(NO, @"\n\t需要实现CDNetModuleRequestDelegate的接口来处理业务\n");
    }
    [[CDNetModuleSetup sharedInstance].delegate requestFinishHandleWithData:json success:success failure:failure];
}

+ (void)handleFailure:(BOOL)isPost
                 path:(NSString*)path
           parameters:(NSDictionary*)parameters
            operation:(NSURLSessionDataTask *)op
                error:(NSError*)error
              success:(void (^)(ZCErrorCode status, NSString * msg, NSDictionary * data))success
              failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure {
    if([error.domain isEqualToString:@"NSURLErrorDomain"]) {//增加上传取消操作后的error code 判断
        failure(9000, @"网络请求取消", nil);
        return;
    }
    failure(8000, @"网络错误", nil);
}


// MARK: - POST

+ (NSURLSessionDataTask *)postRequest:(NSString*)path
                           parameters:(id)parameters
                             progress:(void (^)(NSProgress * _Nonnull))uploadProgress
                              success:(void (^)(ZCErrorCode status, NSString * msg, NSDictionary * data))success
                              failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure {
    path = [MainPath stringByAppendingString:path];
    NSString *baseURLStr = [SKBaseWebService selectBaseURLWithPath:path];
    NSURLSessionDataTask *task = [[SKBaseWebService webService]
                                  postWithBaseUrl:baseURLStr
                                  path:path
                                  parameters:parameters
                                  progress:uploadProgress
                                  success:^(NSURLSessionDataTask *task, id responseObject) {
        [SKBaseWebService handleSuccess:YES
                                   path:path
                             parameters:parameters
                              operation:task
                                   json:responseObject
                                success:success
                                failure:failure];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SKBaseWebService handleFailure:YES
                                   path:path
                             parameters:parameters
                              operation:task
                                  error:error
                                success:success
                                failure:failure];
    }];
    
    
    return task;
}


#pragma mark - GET

+ (NSURLSessionDataTask *)getRequest:(NSString*)path
                          parameters:(NSDictionary*)parameters
                            progress:(void (^)(NSProgress * _Nonnull))downloadProgress
                             success:(void (^)(ZCErrorCode status, NSString * msg, NSDictionary * data))success
                             failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure {
    path = [MainPath stringByAppendingString:path];
    NSString *baseURLStr = [SKBaseWebService selectBaseURLWithPath:path];
    
    NSURLSessionDataTask *task = [[SKBaseWebService webService]
                                  getWithBaseUrl:baseURLStr
                                  path:path
                                  parameters:parameters
                                  progress:downloadProgress
                                  success:^(NSURLSessionDataTask *task, id responseObject) {
        [SKBaseWebService handleSuccess:NO
                                   path:path
                             parameters:parameters
                              operation:task
                                   json:responseObject
                                success:success
                                failure:failure];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        
        [SKBaseWebService handleFailure:NO
                                   path:path
                             parameters:parameters
                              operation:task
                                  error:error
                                success:success
                                failure:failure];
    }];
    
    return task;
}


#pragma mark - UPLOAD

+ (NSURLSessionDataTask *)uploadRequest:(NSString*)path
                             parameters:(NSDictionary*)parameters
                          formDataArray:(NSArray *)formDataArray
                               progress:(nullable void (^)(NSProgress * _Nonnull))uploadProgress
                                success:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary *data))success
                                failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary *data))failure {
    path = [MainPath stringByAppendingString:path];
    NSString *baseURLStr = [SKBaseWebService selectBaseURLWithPath:path];
    parameters = nil; // Don't send paramenter, If send parameters, the server returns error
    
    NSURLSessionDataTask *task = [[SKBaseWebService webService]
                                  uploadWithBaseUrl:baseURLStr
                                  path:path
                                  parameters:parameters
                                  formDataArray:formDataArray
                                  progress:uploadProgress
                                  success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        [SKBaseWebService handleSuccess:YES
                                   path:path
                             parameters:parameters
                              operation:task
                                   json:responseObject
                                success:success
                                failure:failure];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [SKBaseWebService handleFailure:YES
                                   path:path
                             parameters:parameters
                              operation:task
                                  error:error
                                success:success
                                failure:failure];
    }];
    
    
    
    
    return task;
}



#pragma mark - PUT

+ (NSURLSessionDataTask *)putRequest:(NSString*)path
                          parameters:(NSDictionary*)parameters
                             success:(void (^)(ZCErrorCode status, NSString * msg, NSDictionary * data))success
                             failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure
{
    path = [MainPath stringByAppendingString:path];
    NSString *baseURLStr = [SKBaseWebService selectBaseURLWithPath:path];
    
    
    NSURLSessionDataTask *task = [[SKBaseWebService webService]
                                  putWithBaseUrl:baseURLStr
                                  path:path
                                  parameters:parameters
                                  success:^(NSURLSessionDataTask *task, id responseObject) {
        [SKBaseWebService handleSuccess:YES
                                   path:path
                             parameters:parameters
                              operation:task
                                   json:responseObject
                                success:success
                                failure:failure];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SKBaseWebService handleFailure:YES
                                   path:path
                             parameters:parameters
                              operation:task
                                  error:error
                                success:success
                                failure:failure];
    }];
    
    
    return task;
}


#pragma mark - DELETE

+ (NSURLSessionDataTask *)deleteRequest:(NSString*)path
                             parameters:(NSDictionary*)parameters
                                success:(void (^)(ZCErrorCode status, NSString * msg, NSDictionary * data))success
                                failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure {
    path = [MainPath stringByAppendingString:path];
    NSString *baseURLStr = [SKBaseWebService selectBaseURLWithPath:path];
    
    NSURLSessionDataTask *task = [[SKBaseWebService webService]
                                  deleteWithBaseUrl:baseURLStr
                                  path:path
                                  parameters:parameters
                                  success:^(NSURLSessionDataTask *task, id responseObject) {
        [SKBaseWebService handleSuccess:YES
                                   path:path
                             parameters:parameters
                              operation:task
                                   json:responseObject
                                success:success
                                failure:failure];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [SKBaseWebService handleFailure:YES
                                   path:path
                             parameters:parameters
                              operation:task
                                  error:error
                                success:success
                                failure:failure];
    }];
    
    return task;
}

// MARK: - THIRDURL
+ (NSURLSessionDataTask *)thirdRequest:(NSString *)path
                                  urls:(NSString *)url
                                  post:(BOOL)ispost
                            parameters:(NSDictionary*)parameters
                              progress:(void (^)(NSProgress * _Nonnull))downloadProgress
                               success:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))success
                               failure:(void (^)(ZCErrorCode status, NSString *msg, NSDictionary * data))failure {
    
    NSString *baseURLStr = url;
    //parameters = [SKBaseWebService signDictionary:parameters];
    baseURLStr = [url stringByAppendingString:path];
    
    NSURLSessionDataTask *task = nil;
    if (ispost) {
        task = [[SKBaseWebService webService]
                postWithBaseUrl:baseURLStr
                path:path
                parameters:parameters
                progress:downloadProgress
                success:^(NSURLSessionDataTask *task, id responseObject) {
            NSLog(@"+++++++++++++++++++++");
            NSLog(@"%@",task.currentRequest.URL);
            [SKBaseWebService handleSuccess:NO
                                       path:path
                                 parameters:parameters
                                  operation:task
                                       json:responseObject
                                    success:success
                                    failure:failure];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            NSLog(@"+++++++++++++++++++++");
            NSLog(@"%@",task.currentRequest.URL);
            
            [SKBaseWebService handleFailure:NO
                                       path:path
                                 parameters:parameters
                                  operation:task
                                      error:error
                                    success:success
                                    failure:failure];
        }];
        
    } else {
        task = [[SKBaseWebService webService]
                getWithBaseUrl:baseURLStr
                path:path
                parameters:parameters
                progress:downloadProgress
                success:^(NSURLSessionDataTask *task, id responseObject) {
            [SKBaseWebService handleSuccess:NO
                                       path:path
                                 parameters:parameters
                                  operation:task
                                       json:responseObject
                                    success:success
                                    failure:failure];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            
            [SKBaseWebService handleFailure:NO
                                       path:path
                                 parameters:parameters
                                  operation:task
                                      error:error
                                    success:success
                                    failure:failure];
        }];
        
    }
    return task;
}

+ (NSString *)selectBaseURLWithPath:(NSString *)pathStr {
    CDNetModuleSetup *sp = [CDNetModuleSetup sharedInstance];
    if (nil == sp.baseURI || sp.baseURI.length == 0) {
        NSAssert(NO, @"\n\n\t未设置服务器地址 先初始化 CDNetModuleSetup netModuleSetupWithHosts:]\n\n");
    }
    return [sp.baseURI stringByAppendingString:pathStr];
}

@end
