//
//  PEWebService.m
//  ChronicDiseaseManager
//
//  Created by CDCT on 2017/10/17.
//  Copyright © 2017年 cdct. All rights reserved.
//
#import "SKWebService.h"
#import "CDNetModuleSetup.h"

@interface SKWebService()

/** key is urlString, value is AFHTTPSessionManager */
@property (nonatomic, strong) NSMutableDictionary *sessionManagerMDic;

@property (nonatomic, strong) NSString *userAgentStr;
@property (nonatomic, strong) NSString *deviceIDStr;

@property (nonatomic, strong) NSDictionary *header;

@end


@implementation SKWebService

- (instancetype)initWebServiceWithUserAgent:(NSString *)userAgentStr
                                   deviceID:(NSString *)deviceIDStr
{
    if (self = [super init]) {
        self.userAgentStr = userAgentStr;
        self.deviceIDStr  = deviceIDStr;
    }
    return self;
}

static AFHTTPSessionManager *SessionManager ;

- (AFHTTPSessionManager *)sharedHTTPSession {
    if (!SessionManager) {
        SessionManager = [AFHTTPSessionManager manager];
        SessionManager.requestSerializer.timeoutInterval = 30;
        SessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
                                                                    @"text/plain", @"text/json", @"text/javascript",
                                                                    @"text/html", nil];
        SessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        NSDictionary *d = [CDNetModuleSetup sharedInstance].header;
        if (d) {
            NSArray *keys = [d allKeys];
            for (NSString *key in keys) {
                [SessionManager.requestSerializer setValue:d[key] forHTTPHeaderField:key];
            }
        }
    }
    NSArray *a = [CDNetModuleSetup sharedInstance].resetHeaderKeys;
    for (NSString *key in a) {
        [SessionManager.requestSerializer setValue:nil forHTTPHeaderField:key];
    }
    return SessionManager;
}

- (AFHTTPSessionManager *)createSessionManagerWithUrl:(NSString *)urlString
{
    AFHTTPSessionManager *sharedClient = [self sharedHTTPSession];
    return sharedClient;
}

// MARK: -  Public Methods

- (void)updateRequestSerializerHeadFieldWithDic:(NSDictionary *)dic baseURL:(NSString *)baseUrlStr
{
    AFHTTPSessionManager *manager = [self createSessionManagerWithUrl:baseUrlStr];
    
    for (NSString *key in [dic allKeys]) {
        [manager.requestSerializer setValue:[dic objectForKey:key]
                         forHTTPHeaderField:key];
    }
}

// MARK: -  POST

- (NSURLSessionDataTask *)postWithBaseUrl:(NSString *)baseUrlStr
                                     path:(NSString *)path
                               parameters:(id)parameters
                                 progress:(void (^)(NSProgress * _Nonnull))uploadProgress
                                  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [self createSessionManagerWithUrl:baseUrlStr];
    
    if (nil == parameters) {
        parameters = [[NSDictionary alloc] init];
    }
    
    NSURLSessionDataTask *task = [manager POST:baseUrlStr
                                    parameters:parameters
                                       headers:self.header
                                      progress:uploadProgress
                                       success:success
                                       failure:failure];
    
    return task;
}


// MARK: -  GET

- (NSURLSessionDataTask *)getWithBaseUrl:(NSString *)baseUrlStr
                                    path:(NSString *)path
                              parameters:(NSDictionary *)parameters
                                progress:(void (^)(NSProgress * _Nonnull))downloadProgress
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [self createSessionManagerWithUrl:baseUrlStr];
    
    if (nil == parameters) {
        parameters = [[NSDictionary alloc] init];
    }
    
    NSURLSessionDataTask *task = [manager GET:baseUrlStr
                                   parameters:parameters
                                      headers:self.header
                                     progress:downloadProgress
                                      success:success
                                      failure:failure];
    
    return task;
}


// MARK: -  UPLOAD

- (NSURLSessionDataTask *)uploadWithBaseUrl:(NSString *)baseUrlStr
                                       path:(NSString *)path
                                 parameters:(NSDictionary *)parameters
                              formDataArray:(NSArray *)formDataArray
                                   progress:(nullable void (^)(NSProgress * _Nonnull))uploadProgress
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [self createSessionManagerWithUrl:baseUrlStr];
    
    if (nil == parameters) {
        parameters = [[NSDictionary alloc] init];
    }
    
    NSURLSessionDataTask *task = [manager POST:baseUrlStr
                                    parameters:parameters
                                       headers:self.header
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:[formDataArray objectAtIndex:0]
                                    name:[formDataArray objectAtIndex:1]
                                fileName:[formDataArray objectAtIndex:2]
                                mimeType:[formDataArray objectAtIndex:3]];
    }
                                      progress:uploadProgress
                                       success:success
                                       failure:failure];
    
    return task;
}


// MARK: -  PUT

- (NSURLSessionDataTask * )putWithBaseUrl:(NSString *)baseUrlStr
                                     path:(NSString *)path
                               parameters:(NSDictionary *)parameters
                                  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [self createSessionManagerWithUrl:baseUrlStr];
    
    if (nil == parameters) {
        parameters = [[NSDictionary alloc] init];
    }
    
    NSURLSessionDataTask *task = [manager PUT:baseUrlStr
                                   parameters:parameters
                                      headers:self.header
                                      success:success failure:failure];
    
    return task;
    
}

// MARK: -  DELETE

- (NSURLSessionDataTask *)deleteWithBaseUrl:(NSString *)baseUrlStr
                                       path:(NSString *)path
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [self createSessionManagerWithUrl:baseUrlStr];
    
    if (nil == parameters) {
        parameters = [[NSDictionary alloc] init];
    }
    
    NSURLSessionDataTask *task = [manager DELETE:baseUrlStr
                                      parameters:parameters
                                         headers:self.header
                                         success:success failure:failure];
    
    return task;
    
}


// MARK: -  Setter Getter Methods

- (NSMutableDictionary *)sessionManagerMDic
{
    if (nil == _sessionManagerMDic) {
        _sessionManagerMDic = [[NSMutableDictionary alloc] initWithCapacity:5];
    }
    
    return _sessionManagerMDic;
}


- (NSDictionary *)header {
//    if (_header == nil) {
//        NSMutableDictionary *header = [[NSMutableDictionary alloc] init];
//        [header setValue:@"0" forKey:@"platform"];
//        [header setValue:@"wzg" forKey:@"merchant"];
//        [header setValue:@"2" forKey:@"Live-Platform"];
//        [header setValue:@"iPhone" forKey:@"User-Agent"];
//        [header setValue:self.deviceIDStr forKey:@"device_id"];
//        _header = header;
//    }
    return [CDNetModuleSetup sharedInstance].header;
}

@end

