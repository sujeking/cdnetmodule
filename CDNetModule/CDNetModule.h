//
//  CDNetModule.h
//  CDNetModule
//
//  Created by sujeking on 2022/7/29.
//

#import <Foundation/Foundation.h>
//! Project version number for CDNetModule.
FOUNDATION_EXPORT double CDNetModuleVersionNumber;

//! Project version string for CDNetModule.
FOUNDATION_EXPORT const unsigned char CDNetModuleVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CDNetModule/PublicHeader.h>

#import <CDNetModule/SKBaseWebService.h>
#import <CDNetModule/CDNetModuleSetup.h>
